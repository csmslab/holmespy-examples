"""
This example shows how to set up states and basic policies, and run simulations
in order to assess the performance of various policies on a toy problem.
"""

from holmespy.testfunctions import QuadraticGenerator1D, \
    LookupTablePriorGenerator
from holmespy.alternativesets import DiscreteAlternativeSet
from holmespy.policies import ExplorationPolicy, ExploitationPolicy,\
        MaxVariancePolicy, UCBPolicy, MCTSPolicy
from holmespy.state import BayesianOptimizationState
from holmespy.optimizers import DiscreteOptimizer
from holmespy.simulator import Simulator, SimulationParameters, \
        calculate_oc

import numpy as np
import matplotlib.pyplot as plt


"""
Define parameters
"""

NOISE_VAR = 1**2
NUM_ACTIONS = 8
NUM_EXPERIMENTS = 10
NUM_SIMS =  100


"""
Generate prior beliefs
"""
X = DiscreteAlternativeSet(np.linspace(0, 1, NUM_ACTIONS))
prior_gen = LookupTablePriorGenerator(X)
B0 = prior_gen.generate_from_kernel(0, 1, 0.3)

# plot some example sample truths
XX = [x for x in X]
for n in range(10):
    f = B0.sample()
    y = [f(x) for x in X]
    plt.plot(XX, y)

plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.savefig('vanilla_bayesian_optimization_example__truths.pdf')


"""
Define initial state
"""

# constant noise variance model
def sigma2W(x):
    return SIGMA2W

# Define final reward = max of f^N
optimizer = DiscreteOptimizer(X)
def final_reward(B):
    [max_y, max_x] = optimizer.optimize(B.mean_f())
    return max_y

# Initial state
params = BeliefStateParameters(NUM_EXPERIMENTS, X,  sigma2W, final_reward)
S0 = BeliefPriorState(B0, 0, params)


"""
Define some policies
"""

xplr = ExplorationPolicy()
xplt = ExploitationPolicy(optimizer)
maxv = MaxVariancePolicy(optimizer)
ucbp = UCBPolicy(optimizer)

# MCTS Policy
mcts_thresholds = MCTSPolicy.MCTSThresholds( K_PRIOR_THRESH,
        ALPHA_PRIOR_THRESH, K_POSTDECISION_THRESH, ALPHA_POSTDECISION_THRESH)
mcts = MCTSPolicy(COMP_BUDGET, mcts_thresholds, sigma2W, 
        c_ucb = C_UCB,
        final_policy_type = "exploit")
policies = [xplr, xplt, maxv, ucbp, mcts]
policy_labels = ["Exploration", "Exploitation", "Max Variance", "UCB", "MCTS"]

"""
Calculate average time for policy suggestion
"""
import time
t0 = time.time()
for n in range(100):
    mcts.suggest(S0)
t1 = time.time()
total_time_per_suggest = (t1-t0)/100
print("total time per suggest = %f"%total_time_per_suggest)

"""
Set-up and run simulations
"""

# Run simulations over different policies
simulator = Simulator(NUM_SIMS, B0, verbose=True)
all_sim_res = []
for (i,pi) in enumerate(policies):
    print("Policy: " + policy_labels[i])
    sim_params = SimulationParameters(S0, pi, sigma2W)
    sim_res = simulator.simulate(sim_params)
    all_sim_res.append(sim_res)


"""
Analyze simualtion results using OC plots
"""

# calculate OC for all policies, simulations and experiments
OC = np.zeros([len(policies), NUM_SIMS, NUM_EXPERIMENTS+1])
for i,sim_res in enumerate(all_sim_res):
    OC[i,:,:] = calculate_oc(sim_res, optimizer)

# average over all the NUM_SIMULATIONS for each policy and experiment number
median_OC = np.median(OC, axis = 1)
lo_OC = np.percentile(OC, 33, axis = 1)
hi_OC = np.percentile(OC, 66, axis = 1)

plt.figure()

NN = range(NUM_EXPERIMENTS + 1)
plt.plot(NN, median_OC.T)
plt.fill_between(NN, lo_OC[4,:], hi_OC[4,:], color = 'k', alpha = 0.1)

##plt.gca().set_prop_cycle(None)
##for i in range(len(policies)):
##    plt.fill_between(NN, lo_OC[i,:], hi_OC[i,:], alpha = 0.1)

plt.legend(policy_labels)
plt.grid()
plt.savefig('vanilla_bayesian_optimization_example__mean_oc.pdf')
