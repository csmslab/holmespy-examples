"""
This examples shows how to set up the Linear Belief Model and MCTS Policy to
suggest an experiment 
"""

from holmespy.testfunctions import QuadraticGenerator1D
from holmespy.alternativesets import DiscreteAlternativeSet
from holmespy.beliefmodels import LinearBeliefModel
from holmespy.optimizers import DiscreteOptimizer
from holmespy.state import BeliefStateParameters, BeliefPriorState
from holmespy.policies import MCTSPolicy

import matplotlib.pyplot as plt
import numpy as np

"""
Define some parameters
"""

# The number of experiments/alternatives/actions
NUM_ACTIONS = 100
# How many samples we'll use to generate priors
NUM_SAMPLES_FOR_PRIORS = 100
# The number of monomials we'll use for basis functions to the linear model
NUM_BASIS_FUNCTIONS = 3
# The constant noise variance
SIGMA2W = 1.5**2
# The number of experiments we'll carry out during the campaign
NUM_EXPERIMENTS = 10
# The computational budget for MCTS, i.e. the number of nodes to add in tree
COMP_BUDGET = 100
# The number of experimental outcomes we'll simulate for each post-decision
# state in the MCTS policy
E_THR = 5


"""
Form the set of alternatives 
"""
X = DiscreteAlternativeSet(np.linspace(0, 1, NUM_ACTIONS))


"""
Generate priors by samples
"""

# Generate samples of the response
generator = QuadraticGenerator1D([0, 0.5], [0, 1], [-10, -5])
samples = np.zeros([NUM_SAMPLES_FOR_PRIORS, NUM_ACTIONS])
for n in range(NUM_SAMPLES_FOR_PRIORS):
    f = generator.generate()
    samples[n,:] = [f(x) for x in X]

# Form mean and covariance for response
mu_y = np.mean(samples, axis = 0)
Sigma_y = np.cov(samples, rowvar=False)

# Define some basis functions using lambdas.  here we'll use taylor series
# expansion up to degree NUM_BASIS_FUNCTIONS - 1
basis_functions = [lambda x, n=i: x**n for i in range(NUM_BASIS_FUNCTIONS)]

# form design matrix
Phi = np.zeros([NUM_ACTIONS, NUM_BASIS_FUNCTIONS])
for i, x in enumerate(X):
    Phi[i,:] = np.array([f(x) for f in basis_functions])

# We can use Phi, mu_y, and Sigma_y to form priors on coefficients assuming
# linear model
mu_theta = np.linalg.lstsq(Phi, mu_y, rcond=False)[0]
B = np.linalg.lstsq(Phi, Sigma_y, rcond=False)[0]
Sigma_theta = np.linalg.lstsq(Phi, B.T, rcond=False)[0]

# Form linear belief model
B0 = LinearBeliefModel(mu_theta, Sigma_theta, basis_functions = basis_functions)


"""
Plot some samples from this belief model
"""

# Plot some samples from this belief model, 
XX = [x for x in X]
for n in range(10):
    f = B0.sample()
    y = [f(x) for x in X]
    plt.plot(XX, y)

# Plot samples from the samles used to form priors as well
for n in range(10):
    plt.plot(XX, samples[n,:], linestyle=':')

plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.savefig('linear_belief_model_example__truths.pdf')


"""
Set up states for use iwth MCTS
"""

# Noise model = constant noise
def sigma2W_f(x):
    return SIGMA2W

# Define final reward
optimizer = DiscreteOptimizer(X)
def final_reward(B):
    [max_y, max_x] = optimizer.optimize(B.mean_f())
    return max_y

# Set up state params and initial state
params = BeliefStateParameters(NUM_EXPERIMENTS, X, sigma2W_f, final_reward)
S0 = BeliefPriorState(B0, 0, params)


"""
Use the MCTS Policy to select the next experiment
"""

# Define a policies
mcts = MCTSPolicy(COMP_BUDGET, E_THR)

# make a suggestion
x = mcts.suggest(S0)
print("Suggested action = " + str(x))
