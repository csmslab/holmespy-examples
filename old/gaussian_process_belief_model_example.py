"""
This example shows how to use the GaussianProcessBeliefModel.
"""

from holmespy.testfunctions import QuadraticGenerator1D
from holmespy.alternativesets import ContinuousAlternativeSet
from holmespy.beliefmodels import GaussianProcessBeliefModel
from holmespy.optimizers import ContinuousOptimizer
from holmespy.state import BeliefStateParameters, BeliefPriorState
from holmespy.policies import UCBPolicy

from sklearn.gaussian_process.kernels import RBF

import matplotlib.pyplot as plt
import numpy as np

"""
Define some parameters
"""

# The number of experiments/alternatives/actions
NUM_ACTIONS = 100
# How many samples we'll use to generate priors
NUM_SAMPLES_FOR_PRIORS = 100
# The constant noise variance
SIGMA2W = 0.01**2
# The number of experiments we'll carry out during the campaign
NUM_EXPERIMENTS = 10


"""
Utility function to plot mean and uncertainty envelope)
"""
def plot_beliefs(B, X_plot):
    # plot mean and uncertainty envelope
    f_mean = B.mean_f()
    y = np.array([f_mean(x) for x in X_plot])
    std = np.sqrt([B.covariance(x, x) for x in X_plot])

    for k in np.linspace(1, 3, 10):
        plt.fill_between(X_plot.flatten(), 
            y-k*std, y+k*std, alpha = 0.05, color='k', linewidth=0.5)
    plt.plot(X_plot, y, linewidth=3)


"""
Form the set of alternatives 
"""
X = ContinuousAlternativeSet([(0,1)])


"""
Generate priors by defining kernel and mean value
"""
# Define the mean and covariance kernel
mu_0 = 10 
kernel = RBF(length_scale = 0.1, length_scale_bounds = (1e-2, 0.5))
support = np.linspace(0, 1, 10)[:, np.newaxis]
B0 = GaussianProcessBeliefModel(mu_0, kernel, support = support,
        num_hyperparameter_fits = 2)


"""
Plot some samples from this belief model
"""

# Arrays of "x" values to the GaussianProcessBeliefModel must be
# two dimensional numpy arrays, regardless of the dimensionality of the "x"
# That is, even for scalar "x", we require such inputs to be rows in
# a n x 1 matrix
X_plot = np.linspace(0, 1, 100)[:, np.newaxis]

# Sample and plot samples
for n in range(10):
    # Sample, using the provided data points as the support to define
    # the interpolated function (using a Guassian Process).
    f = B0.sample()
    y = [f(x) for x in X_plot]
    plt.plot(X_plot, y)

# plot the beliefs as well
plot_beliefs(B0, X_plot)
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.savefig('gaussian_process_belief_model_example__samples_and_mean.pdf')


"""
Apply any preliminary data
"""

# apply 5 data points based on this ground truth
f_star = lambda x: mu_0 + 0.25 - 5*(x[0]-0.5)**2
x_data = [X.sample() for i in range(3)]
y_data = [f_star(x)  + np.random.normal(0, np.sqrt(SIGMA2W)) for x in x_data]
for x, y in zip(x_data, y_data):
    B0 = B0.update(x, y, SIGMA2W)

# plot the updated beliefs , and samples
plt.figure()

# plot updated samples
for n in range(10):
    f = B0.sample()
    y = [f(x) for x in X_plot]
    plt.plot(X_plot, y)

# plot mean and uncertainty envelope
plot_beliefs(B0, X_plot)

# plot data
plt.scatter(x_data, y_data, s=30)
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.savefig(
    'gaussian_process_belief_model_example__updated_samples_and_mean.pdf')


"""
Set up states for use with policies
"""

# Noise model = constant noise
def sigma2W_f(x):
    return SIGMA2W

# Define final reward
optimizer = ContinuousOptimizer(X)
def final_reward(B):
    [max_y, max_x] = optimizer.optimize(B.mean_f())
    return max_y

# Set up state params and initial state
params = BeliefStateParameters(NUM_EXPERIMENTS, X, sigma2W_f, final_reward)
S0 = BeliefPriorState(B0, 0, params)

"""
Use the UCB Policy to select the next experiment
"""

# Define the policy
pi = UCBPolicy(optimizer)

# make a suggestion
x = pi.suggest(S0)
print("Suggested action = " + str(x))
