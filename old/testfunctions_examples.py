"""
Examples of how to use the classes in testfunctions.py in order to quickly
generate functions and priors to test policies or models.
"""

import numpy as np
import matplotlib.pyplot as plt

from holmespy.testfunctions import LookupTablePriorGenerator, \
        Quadratic1DSampler
from holmespy.alternativesets import DiscreteAlternativeSet


def plot_belief(X, B, num_samples_to_plot = 5):
    y = np.array([ B.mean(x) for x in X])
    yerr = np.array([ B.std(x) for x in X])

    plt.plot(X_arr, y, color='black', linewidth='3')
    plt.fill_between(X_arr, y + 2*yerr, y - 2*yerr, color='black', alpha = 0.07)
    plt.fill_between(X_arr, y + yerr, y - yerr, color='black', alpha = 0.07)

    for n in range(num_samples_to_plot):
        sample_f = B.sample_f()
        y = [sample_f(x) for x in X]
        plt.plot(X_arr, y)



"""
Generate a lookup-table prior from samples of quadratic functions
"""

# First we form the discrete set of alternatives
X_arr = np.arange(-10, 10, 0.5)
X = DiscreteAlternativeSet(X_arr)

# Next we build a sampler to generate continuous quadratic functios based on
# uniform sampling of roots. Below, we'll sample quadratic functions 
# f(x) = a(x-r1)(x-r2) by uniformly sampling r1 between -1 and 0, r2 between 
# 0 and 1, and a between 0 and 2
sampler = Quadratic1DSampler([-3,-2], [0, 1], [0,2])

# We'll use this to build a lookup table priors on the values of x in X
prior_generator = LookupTablePriorGenerator(X)
B_0 = prior_generator.generate_from_sampler(sampler)

# We can plot mean, and standard deviations for this prior
plot_belief(X, B_0)
plt.savefig('testfunctions_examples0.pdf')
plt.close()



"""
Generate a lookup-table prior with constant mean and guassian kernel. All
examples have prescribed mean value function = 0.
"""

# Short lengthscale, small variance
B_1a = prior_generator.generate_from_kernel(0, 2*2, 1)
plot_belief(X, B_1a)
plt.savefig('testfunctions_examples1a.pdf')
plt.close()

# Short lengthscale, large variance
B_1b = prior_generator.generate_from_kernel(0, 10*10, 1)
plot_belief(X, B_1b)
plt.savefig('testfunctions_examples1b.pdf')
plt.close()

# Long lengthscale, small variance
B_1c = prior_generator.generate_from_kernel(0, 2*2, 2)
plot_belief(X, B_1c)
plt.savefig('testfunctions_examples1c.pdf')
plt.close()
