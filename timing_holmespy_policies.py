from holmespy.policies import EIPolicy
from holmespy.testfunctions import generate_one_dimensional_problem
from holmespy.simulator import Simulator, generate_bayesian_optimization_states

NUM_SIMS = 2

problem = generate_one_dimensional_problem()
ground_truth_states = \
    generate_bayesian_optimization_states(problem['initial_state'], NUM_SIMS)
simulator = Simulator(ground_truth_states, problem['observation_model'])
pi = EIPolicy(problem['optimizer'], 
        problem['observation_model'], include_noise_var = False)

simulator.simulate(problem['initial_state'], problem['observation_model'], pi)
