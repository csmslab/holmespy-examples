"""
This example shows how to set up states and basic policies, and run simulations
in order to assess the performance of various policies on a toy problem.
"""

from holmespy.testfunctions import LookupTablePriorGenerator
from holmespy.alternativesets import DiscreteAlternativeSet
from holmespy.policies import ExplorationPolicy, ExploitationPolicy,\
        MaxVariancePolicy, UCBPolicy, MCTSPolicy, EIPolicy
from holmespy.state import BayesianOptimizationState, KeepHistory
from holmespy.optimizers import DiscreteOptimizer
from holmespy.simulator import Simulator, SimulationParameters, \
        calculate_oc

import numpy as np
import matplotlib.pyplot as plt



"""
Define parameters
"""

NOISE_VAR = 1**2
NUM_ACTIONS = 8
NUM_EXPERIMENTS = 10
NUM_SIMS =  100

actions = DiscreteAlternativeSet(np.linspace(0, 1, NUM_ACTIONS))
optimizer = DiscreteOptimizer(actions)


"""
Build prior belief and state
"""

prior_generator = LookupTablePriorGenerator(actions)
prior_belief = prior_generator.generate_from_kernel(0, 1, 0.3)

# constant noise variance model
def constant_noise_var_f(action):
    return NOISE_VAR

params = { 
    "experimental_budget": NUM_EXPERIMENTS,
    "optimizer": optimizer,
    "actions": actions,
    "noise_variance_f": constant_noise_var_f
}

# Use KeepHistory mixin to keep experimental history
class MyState(KeepHistory, BayesianOptimizationState):
    pass

initial_state = MyState(prior_belief, 0, params)


"""
Define some policies
"""

xplr = ExplorationPolicy()
xplt = ExploitationPolicy(optimizer)
maxv = MaxVariancePolicy(optimizer)
ucbp = UCBPolicy(optimizer)
ei = EIPolicy(optimizer, constant_noise_var_f)
policies = [xplr, xplt, maxv, ucbp, ei]


"""
Set-up and run simulations
"""

# Run simulations over different policies
simulator = Simulator(NUM_SIMS, prior_belief, verbose=True)
all_sim_res = []
for pi in policies:
    print("Policy: " + str(pi))
    sim_params = SimulationParameters(initial_state, pi, constant_noise_var_f)
    sim_res = simulator.simulate(sim_params)
    all_sim_res.append(sim_res)


"""
Analyze simualtion results using OC plots
"""
OC = np.zeros([len(policies), NUM_SIMS, NUM_EXPERIMENTS+1])
for i, sim_res in enumerate(all_sim_res):
    OC[i,:,:] = calculate_oc(sim_res, optimizer)

median_OC = np.median(OC, axis = 1)
lo_OC = np.percentile(OC, 33, axis = 1)
hi_OC = np.percentile(OC, 66, axis = 1)

NN = range(NUM_EXPERIMENTS + 1)

plt.figure()
plt.plot(NN, median_OC.T)
plt.legend([str(pi) for pi in policies])
plt.grid()
plt.savefig('vanilla_bayesian_optimization_examples__mean_oc.pdf')
