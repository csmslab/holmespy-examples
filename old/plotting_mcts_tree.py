from holmespy.testfunctions import QuadraticGenerator1D, \
    LookuptablePriorGenerator
from holmespy.alternativesets import DiscreteAlternativeSet
from holmespy.policies import MCTSPolicy
from holmespy.state import BeliefPriorState, BeliefStateParameters
from holmespy.optimizers import DiscreteOptimizer

from mctsutils import viz, stats

import numpy as np
import matplotlib.pyplot as plt


"""
Define parameters
"""

# The constant noise variance
SIGMA2W = 1**2
# The number of experiments in the experimental campaign
NUM_EXPERIMENTS =25 
# The computational budget for MCTS, i.e. the number of nodes to add to tree
COMP_BUDGET = 256
# The number of outcomes to simulate for each post-decision state in the 
# MCTS policy
E_THR = 8 
# The number of experiments/alternatives/actions 
NUM_ACTIONS = 8
# The number of samples to use to generate priors
NUM_SAMPLES_FOR_PRIORS = 100


"""
Generate Prior Belief
"""

## Generate Prior belief model
X = DiscreteAlternativeSet(np.linspace(0, 1, NUM_ACTIONS))
XX = [x for x in X]
prior_gen = LookuptablePriorGenerator(
        QuadraticGenerator1D([0, 0.5], [0, 1], [-10, -5]), X)
B0 = prior_gen.generate_priors(NUM_SAMPLES_FOR_PRIORS)


"""
Define initial state for MCTS Policy
"""

# noise model is constant noise
def sigma2W(x):
    return SIGMA2W

# Define final reward = max of f^N
optimizer = DiscreteOptimizer(X)
def final_reward(B):
    [max_y, max_x] = optimizer.optimize(B.mean())
    return max_y

# Initial state
params = BeliefStateParameters(NUM_EXPERIMENTS, X,  sigma2W, final_reward)
S0 = BeliefPriorState(B0, 0, params)


"""
Use MCTS Policy to make a suggestion, and build scenario tree
"""

# make a suggestion to populate scenario tree
mcts = MCTSPolicy(COMP_BUDGET, E_THR)
mcts.suggest(S0)

# The root node of the scenario tree
v_0 = mcts.v_0


"""
Analyze and print resulting scenario tree
"""

# some tree statistics
print("Num nodes  = " + str(stats.num_nodes(v_0)))
print("Tree depth = " + str(stats.depth(v_0)))
nodes_per_level = stats.nodes_per_level(v_0)
for i, level in enumerate(nodes_per_level):
    print("Level %d: %d" %(i, len(level)))

# plot the tree with various plotting parameters
b_all = np.linspace(-5, 5, 5)
sig_all = np.linspace(0, 0.5, 5)

count = 0
for b in b_all:
    for sig in sig_all:
        plt.subplot(len(b_all), len(sig_all), count+1)
        viz.plot_tree(v_0, b = b, sig = sig, show=False)
        plt.title('b = %f, sig = %f'%(b, sig))
        count = count + 1

# set the image size
plt.gcf().set_size_inches(45, 50)

# save image
plt.savefig('mcts_tree.pdf')
